import React, {useState, useEffect } from "react";
import {View} from 'react-native';
import * as SQLite from "expo-sqlite";

const db = SQLite.openDatabase("SprintCompassDB");

export default db;