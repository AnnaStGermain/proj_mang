import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import {
    Text,
    View,
    Button,
    TextInput,
    TouchableOpacity,
    Alert,
    ScrollView
} from "react-native";
import db from "./Database";
import { useNavigation } from '@react-navigation/native';
import { PassSubIndex } from "./DisplaySubtask";
import styles from "../styles/display_user_story";
let storyID = 1;
function checkNumeric(str) {
    if (typeof (str) !== "string") return false;
    return !isNaN(str) && !isNaN(parseFloat(str))
}

function DisplayUserStory(props) {
    const [id, setID] = useState();
    const [entryIndex, setEntryIndex] = useState(props.projIndex);
    const [userStoryText, setUserStoryText] = useState("");
    const [subTasks, setSubTasks] = useState([""]);
    const [newSubTask, setNewSubTask] = useState("");
    const [description, setDescription] = useState("");
    const [hours_worked, setHoursWorked] = useState("");
    const [hours_Remaining, setHoursRemainingd] = useState("");
    const [sprint, setSprint] = useState(0);
    const [userStories, setUserStories] = useState([]);
    const [subtaskIDs, setSubtaskIDs] = useState([]);
    const [teamMember, setTeamMember] = useState("");
    const [percentComplete, setPercent] = useState("");
    const [originalEstimate, setOriginalEstimate] = useState("");
    const navigation = useNavigation();
    useEffect(() => {
        loadData();
    }, []);

    const goBack = async () => {
        navigation.navigate("Home");
    };

    const handleNewSubTask = (newStory) => {
        setNewSubTask(newStory);
    };

    const handleNewSubTask2 = (newStory) => {
        setPercent(newStory);
    };
    const handleNewSubTask3 = (newStory) => {
        setOriginalEstimate(newStory);
    };
    
    const handleTeamMember = (team) => {
        setTeamMember(team);
    }

    const setIDs = (ids) => {
        setSubtaskIDs(ids);
    };

    const handleUserStoryText = (text) => {
        setUserStoryText(text);
    };

    const handleDescription = (text) => {
        setDescription(text);
    };

    const handleHoursWorked = (text) => {
        setHoursWorked(text);
    };

    const handleHoursRemaining = (text) => {
        setHoursRemainingd(text);
    };

    const handleSprint = (text) => {
        setSprint(text);
    };



    const deleteData = async () => {
        db.transaction((tx) => {
            tx.executeSql(
                `DELETE FROM User_Stories WHERE user_story_id = ${storyID}`,
                [],
                (_, { rowsAffected }) =>
                    rowsAffected > 0
                        ? console.log("ROW DELETED!")
                        : console.log("DELETE FAILED!"),
                (_, result) => console.log("DELETE failed:" + result)
            );
        });

        navigation.navigate("Home");
    };
    const updateData = async () => {
        db.transaction((tx) => {
            // executeSql(sqlStatement, arguments, success, error)
            tx.executeSql(
                `UPDATE User_Stories SET user_story_text = ? WHERE user_story_id = ${storyID}`,
                [userStoryText],
                (_, { rowsAffected }) =>
                    rowsAffected > 0
                        ? console.log("ROW UPDATED!")
                        : console.log("UPDATE FAILED!"),
                (_, result) => console.log("UPDATE failed:" + result)
            );
        });

        reInsertSubtasks(storyID)

    };

    const reInsertSubtasks = async (id) => {
        let list = [];
        let count = 0;
        if(teamMember == "" || newSubTask == "" || description == "" || hours_worked == "" || hours_Remaining == "" || sprint == "")
        {
            Alert.alert("Alert!", `Missing data`, [{ text: "close" }]);
            return;
        }
        subTasks.forEach((item) => {
            if (item != "") {
                db.transaction((tx) => {
                    tx.executeSql(
                        `INSERT INTO Subtasks (user_story_id , team_member_name, name, description, hours_worked, remaining_hours, sprint_num) values (?,?,?,?,?,?,?)`,
                        [
                            id,
                            teamMember,
                            newSubTask,
                            description,
                            hours_worked,
                            hours_Remaining,
                            sprint
                        ],
                        (_, { rowsAffected, insertId }) =>
                            rowsAffected > 0
                                ? list[count] = insertId//Alert.alert('Alert!', `User story added`, [{ text: "close", onPress: () => navigation.navigate('Home') },])
                                : Alert.alert('Alert!', `User story failed to add`, [{ text: "close" }]),
                        (_, result) => console.log("INSERT failed:" + result)
                    );
                });
                count++;
            }
        });
        navigation.navigate("Home");
        //Alert.alert("User Stories Updated!")
    }

    const loadData = async () => {
        db.transaction((tx) => {
            tx.executeSql(
                `SELECT * FROM User_Stories WHERE user_story_id = ${storyID}`,
                [],
                (_, { rows }) => {
                    console.log("ROWS RETRIEVED!");

                    let entries = rows._array;
                    setUserStoryText(entries[0].user_story_text);
                    setPercent(entries[0].percent_complete);
                    setOriginalEstimate(entries[0].original_estimate);
                },
                (_, result) => {
                    console.log("SELECT failed!");
                    console.log(result);
                }
            );
        });
        db.transaction((tx) => {
            tx.executeSql(
                `SELECT * FROM Subtasks WHERE user_story_id = ${storyID}`,
                [],
                (_, { rows }) => {
                    console.log("ROWS RETRIEVED!");

                    let entries = rows._array;
                    console.log(entries);
                    let list = [];
                    let list2 = [];
                    let count = 0;
                    entries.forEach((entry) => {
                        list[count] = entry.name;
                        list2[count] = entry.subtask_id;
                        count++;
                    });
                    setSubTasks(list);
                    setIDs(list2);
                },
                (_, result) => {
                    console.log("SELECT failed!");
                    console.log(result);
                }
            );
        });
    };

    const viewUserStory = async (id) => {
        props.handleUserStoryID(userStories[id])
        navigation.navigate("AddProject");
    };

    const addSubtask = () => {
        if(teamMember == "" || newSubTask == "" || description == "" || hours_worked == "" || hours_Remaining == "" || sprint == "")
        {
            Alert.alert("Alert!", `Missing data`, [{ text: "close" }]);
            return;
        }
        if (newSubTask !== "" || newSubTask !== undefined) {
            reInsertSubtasks(storyID)
            var newArr = subTasks.concat(newSubTask);
            console.log(newArr);
            setSubTasks(newArr);
            Alert.alert("New SubTask Added!");
        }
    }
    return (
        <View style={styles.content}>
            <Text style={styles.headerText}>User Story Information</Text>
            <Text style={styles.textStyle}>User Story Text: </Text>
            <TextInput
                placeholder={"userStoryText"}
                value={userStoryText}
                onChangeText={handleUserStoryText}
                style={styles.textBoxStyle}
            />
            <Text style={styles.textStyle}>User Story Percent: </Text>
            <TextInput
                placeholder={"userStoryText"}
                value={percentComplete}
                onChangeText={handleNewSubTask2}
                style={styles.textBoxStyle}
            />
            <Text style={styles.textStyle}>User Story Original Estimate: </Text>
            <TextInput
                placeholder={"userStoryText"}
                value={originalEstimate}
                onChangeText={handleNewSubTask3}
                style={styles.textBoxStyle}
            />
            <Text style={styles.textStyle}>List Of Subtasks: </Text>
            <ScrollView contentContainerStyle={styles.scrollView}
        style={styles.container}>
                {subTasks.map((item, index) => {
                    return (
                        <View key={index}>
                            <PassSubIndex storyIDs={subtaskIDs[index]} />
                        </View>
                    );
                })}
            </ScrollView>
            <Text style={styles.textStyle}>Enter New Subtask Name: </Text>
            <TextInput
                placeholder={newSubTask}
                value={newSubTask}
                onChangeText={handleNewSubTask}
                style={styles.textBoxStyle}
            />
            <Text style={styles.textStyle}>Enter Description: </Text>
            <TextInput
                placeholder={"description"}
                value={description}
                onChangeText={handleDescription}
                style={styles.textBoxStyle}
            />
            <Text style={styles.textStyle}>Enter Hours Worked: </Text>
            <TextInput
                placeholder={"hours_worked"}
                value={hours_worked.toString()}
                onChangeText={handleHoursWorked}
                style={styles.textBoxStyle}
            />
            <Text style={styles.textStyle}>Enter Hours Remaining: </Text>
            <TextInput
                placeholder={"hours_Remaining"}
                value={hours_Remaining.toString()}
                onChangeText={handleHoursRemaining}
                style={styles.textBoxStyle}
            />
            <Text style={styles.textStyle}>Enter Team Member: </Text>
            <TextInput
                placeholder={"team member"}
                value={teamMember}
                onChangeText={handleTeamMember}
                style={styles.textBoxStyle}
            />
            <Text style={styles.textStyle}>Enter Sprint # (To Be completed in): </Text>
            <TextInput
                placeholder={"sprint"}
                value={sprint.toString()}
                onChangeText={handleSprint}
                style={styles.textBoxStyle}
            />
            <View>
                <View style={{flexDirection: "row", justifyContent:"space-evenly"}}>
            <TouchableOpacity style={styles.buttons} onPress={addSubtask}><Text style={styles.buttonText}>Add Subtask</Text></TouchableOpacity>
            <TouchableOpacity style={styles.buttons} onPress={updateData}><Text style={styles.buttonText}>Update</Text></TouchableOpacity>
            <TouchableOpacity style={styles.buttons} onPress={deleteData}><Text style={styles.buttonText}>Delete</Text></TouchableOpacity>
            <TouchableOpacity style={styles.buttons} onPress={goBack}><Text style={styles.buttonText}>Go Back</Text></TouchableOpacity>
            </View>
            </View>
        </View>
    );
}


export function PassIndex(props) {
    storyID = props.storyIDs;
    const navigation = useNavigation();

    const passID = () => {
        storyID = props.storyIDs;
        navigation.navigate("DisplayUserStory");
    }

    return (<View style={styles.buttonWidth}>
        <TouchableOpacity style={styles.buttons} onPress={() => passID()}><Text style={styles.buttonText}>User Story {storyID}</Text></TouchableOpacity>
    </View>);
};
export default DisplayUserStory;
