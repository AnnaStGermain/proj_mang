import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect, useContext } from "react";
import {
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
} from "react-native";
import db from "./Database";
import { useNavigation } from "@react-navigation/native";
import DisplayUserStory from "./DisplayUserStory";
import { PassIndex } from "./DisplayUserStory";
import { createStackNavigator } from "@react-navigation/stack";
let storyID = 1;
import styles from "../styles/product_backlog_style";
const Stack = createStackNavigator();

function checkNumeric(str) {
  if (typeof str !== "string") return false;
  return !isNaN(str) && !isNaN(parseFloat(str));
}
export default function DisplayProjectBacklog(props) {
  const [id, setID] = useState(props.projDBIndex);
  const [entryIndex, setEntryIndex] = useState(props.projIndex);
  const [relativeEstimate, setRelativeEstimate] = useState("");
  const [estimatedTotal, setEstimatedTotal] = useState("");
  const [userStories, setUserStories] = useState([""]);
  const [newUserStory, setNewUserStory] = useState("");
  const [description, setDescription] = useState("");
  const [error, setError] = useState("");
  const [userStoryIds, setUserStoryIds] = useState([]);
  const [percentComplete, setPercent] = useState("");
  const [originalEstimate, setOriginalEstimate] = useState("");

  const navigation = useNavigation();
  useEffect(() => {
    loadData();
  }, []);

  const goBack = async () => {
    navigation.navigate("Home");
  };

  const handleRelativeEstimate = (relativeEstimate) => {
    setRelativeEstimate(relativeEstimate);
  };

  const handleEstimatedTotal = (estimated) => {
    setEstimatedTotal(estimated);
  };

  const handleNewUserStory2 = (newStory) => {
    setPercent(newStory);
  };

  const handleNewUserStory3 = (newStory) => {
    setOriginalEstimate(newStory);
  };
  const handleNewUserStory = (newStory) => {
    setNewUserStory(newStory);
  };

  const handleUserStories = (stories) => {
    setUserStories(stories);
  };

  const handleUserStoryIds = (ids) => {
    setUserStoryIds(ids);
  };

  const addUserStory = () => {
    if (newUserStory !== "" || newUserStory !== undefined) {
      var newArr = userStories.concat(newUserStory);
      setUserStories(newArr);
    }
  };

  const deleteData = async () => {
    db.transaction((tx) => {
      tx.executeSql(
        `DELETE FROM Product_Backlog WHERE product_backlog_id = ${props.projDBIndex}`,
        [],
        (_, { rowsAffected }) =>
          rowsAffected > 0
            ? console.log("ROW DELETED!")
            : console.log("DELETE FAILED!"),
        (_, result) => console.log("DELETE failed:" + result)
      );
    });

    navigation.navigate("Home");
  };
  const updateData = async () => {
    db.transaction((tx) => {
      // executeSql(sqlStatement, arguments, success, error)
      tx.executeSql(
        `UPDATE Product_Backlog SET initial_relative_estimate = ? , inital_estimated_cost = ? WHERE product_backlog_id = ${props.projDBIndex}`,
        [relativeEstimate, estimatedTotal],
        (_, { rowsAffected }) =>
          rowsAffected > 0
            ? console.log("ROW UPDATED!")
            : console.log("UPDATE FAILED!"),
        (_, result) => console.log("UPDATE failed:" + result)
      );
    });
    //Update user stories

    // //delete all user stories first, and readd them
    // db.transaction((tx) => {
    //   // executeSql(sqlStatement, arguments, success, error)
    //   tx.executeSql(
    //     `DELETE FROM User_Stories WHERE project_backlog_id = ${props.projDBIndex}`,
    //     [],
    //     (_, { rowsAffected }) =>
    //       rowsAffected > 0
    //         ? console.log("User stories removed (for alter)!")
    //         : console.log("UPDATE FAILED!"),
    //     (_, result) => console.log("UPDATE failed:" + result)
    //   );
    // });

    // reInsertUserStories(props.projDBIndex);
  };

  const reInsertUserStories = async () => {
    if(newUserStory == "" || percentComplete == "" || originalEstimate == "")
    {
      Alert.alert("Alert!", `Missing data`, [{ text: "close" }]);
      return;
    }
    db.transaction((tx) => {
      tx.executeSql(
        `INSERT INTO User_Stories (project_backlog_id, user_story_text, percent_complete, original_estimate) values (?,?,?,?)`,
        [props.projDBIndex, newUserStory, percentComplete, originalEstimate],
        (_, { rowsAffected }) =>
          rowsAffected > 0
            ? Alert.alert("Alert!", `User story added`, [
                {
                  text: "close",
                  onPress: () => navigation.navigate("Home"),
                },
              ])
            : Alert.alert("Alert!", `User story failed to add`, [
                { text: "close" },
              ]),
        (_, result) => console.log("INSERT failed:" + result)
      );
    });
  };

  const loadData = async () => {
    let id = 0;
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM Product_Backlog WHERE project_id = ${props.projDBIndex}`,
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          let entries = rows._array;
          console.log(entries);
          if (entries.length > 0) {
            handleRelativeEstimate(entries[0].initial_relative_estimate);
            handleEstimatedTotal(entries[0].inital_estimated_cost);
            id = entries[0].product_backlog_id;
          }
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });
    db.transaction((tx) => {
      let list = [];
      let list2 = [];
      tx.executeSql(
        `SELECT * FROM User_Stories WHERE project_backlog_id = ${id}`,
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          let entries = rows._array;
          let count = 0;
          entries.forEach((entry) => {
            if (entry.project_backlog_id === id) {
              list[count] = entry.user_story_text;
              list2[count] = entry.user_story_id;
              count++;
            }
          });
          handleUserStories(list);
          handleUserStoryIds(list2);
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });
  };

  const viewUserStory = async (id) => {
    storyID = userStoryIds[id];
    navigation.navigate("DisplayUserStory");
  };

  return (
    <View style={styles.content}>
      <Text style={styles.textStyle}>Project Backlog Information</Text>
      <Text>{error}</Text>
      <Text style={styles.textStyle}>Relative Estimate: $</Text>
      <TextInput
        placeholder={relativeEstimate.toString()}
        value={relativeEstimate.toString()}
        onChangeText={handleRelativeEstimate}
        style={styles.textBoxStyle}
      />
      <Text style={styles.textStyle}>Estimated Total Cost: $</Text>
      <TextInput
        placeholder={estimatedTotal.toString()}
        value={estimatedTotal.toString()}
        onChangeText={handleEstimatedTotal}
        style={styles.textBoxStyle}
      />
      <Text style={styles.textStyle}>List Of User Stories: </Text>
      <ScrollView contentContainerStyle={styles.scrollView}
        style={styles.container}>
        {userStories.map((item, index) => {
          return (
            <View key={index}>
              <PassIndex storyIDs={userStoryIds[index]} />
            </View>
          );
        })}
      </ScrollView>
      <View style={styles.buttonWidth}>
      <Text style={styles.textStyle}>Enter New User Story: </Text>
      <TextInput
        placeholder={"newUserStory"}
        value={newUserStory}
        onChangeText={handleNewUserStory}
        style={styles.textBoxStyle}
      />
      <TextInput
        placeholder={"User Percent Complete"}
        value={percentComplete}
        onChangeText={handleNewUserStory2}
        style={styles.textBoxStyle}
      />
      <TextInput
        placeholder={"User Story Original Hours"}
        value={originalEstimate}
        onChangeText={handleNewUserStory3}
        style={styles.textBoxStyle}
      />
 <View style={{flexDirection: "row", justifyContent:"space-evenly"}}>
      <TouchableOpacity style={styles.buttons} onPress={reInsertUserStories}>
        <Text style={styles.buttonText}>Add User Story</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.buttons} onPress={updateData}>
        <Text style={styles.buttonText}>Update</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.buttons} onPress={deleteData}>
        <Text style={styles.buttonText}>Delete</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.buttons} onPress={goBack}>
        <Text style={styles.buttonText}>Go Back</Text>
      </TouchableOpacity>
</View>
      </View>
    </View>
  );
}
