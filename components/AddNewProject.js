import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
} from "react-native";
import styles from "../styles/project_add_style.js";
import db from "./Database";
import { useNavigation } from "@react-navigation/native";
let count2 = 0;
let list2 = [];

function addNewProject(props) {
  const [date, setDate] = useState("");
  const [name, setName] = useState("");
  const [team, setTeam] = useState("");
  const [hours, setHours] = useState(0);
  const [story, setStory] = useState(0);
  const [cost, setCost] = useState(0);
  const [velocity, setVelocity] = useState(0);
  const [description, setDescription] = useState("");
  const [listOfTeamMembers, setListOfTeamMembers] = useState([]);
  const [ifExists, setExists] = useState(false);
  const [listOfAddedTeamMembers, setListOfAddedTeamMembers] = useState([]);
  const [projectInsertId, setProjectInsertId] = useState();
  const [listOfTeamDBIndecies, setListOfTeamDBIndecies] = useState([]);
  const navigation = useNavigation();
  
  useEffect(() => {
    loadTeamMembers();
  }, [props]);

  const setID = (id) => {
    setProjectInsertId(id);
  };

  const formatDate = (date) => {
    setDate(date);
  };

  const handleProject = (name) => {
    setName(name);
  };

  const handleTeam = (team) => {
    setTeam(team);
  };

  const handleHours = (hours) => {
    let number = parseInt(hours);
    setHours(number);
  };

  const handleEstimate = (story) => {
    let number = parseInt(story);
    setStory(number);
  };

  const handleCost = (cost) => {
    let number = parseInt(cost);
    setCost(number);
  };

  const handleVelocity = (velocity) => {
    let number = parseInt(velocity);
    setVelocity(number);
  };

  const handleDescription = (description) => {
    setDescription(description);
  };

  const loadTeamMembers = async () => {
    db.transaction((tx) => {
      tx.executeSql(
        "SELECT * FROM Team_Member",
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          // clear data currently stored
          if (rows._array !== undefined) {
            let entries = rows._array;

            let list = [];
            let list2 = [];
            let count = 0;
            entries.forEach((entry) => {
              let name = entry.first_name + " " + entry.last_name;
              let id = entry.team_member_id;
              let object = {
                name: name,
                id: id,
              };
              list[count] = object;
              list2[count] = entry.team_member_id;
              count++;
            });
            setListOfTeamMembers(list);
            // setListOfTeamDBIndecies(list2);
          }
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });
  };

  const insertData = async () => {
    if(team == "" || name == "" || date == "" || description == "")
    {
      Alert.alert("Alert!", `Missing data`, [{ text: "close" }]);
      return;
    }
    db.transaction((tx) => {
      tx.executeSql(
        `INSERT INTO Projects (team_name, project_name, project_start_date, hours_equivalent_story_point, estimated_story_points, estimated_cost, velocity, project_description) values (?, ?, ?, ?, ?, ?, ?, ?)`,
        [team, name, date, hours, story, cost, velocity, description],
        (_, { rowsAffected, insertId }) => {
          for (let i = 0; i < listOfAddedTeamMembers.length; ++i) {
            db.transaction((tx) => {
              tx.executeSql(
                `INSERT INTO Projects_Team_Members (project_id, team_member_id) values (?, ?)`,
                [insertId, listOfTeamDBIndecies[i]],
                (_, { rowsAffected }) =>
                  rowsAffected > 0
                    ? null
                    : Alert.alert("Alert!", `Entry failed to add`, [
                        { text: "close" },
                      ]),
                (_, result) => console.log("INSERT failed:" + result)
              );
            });
          }
          rowsAffected > 0
            ? Alert.alert("Alert!", `Entry added`, [{ text: "close" }])
            : Alert.alert("Alert!", `Entry failed to add`, [{ text: "close" }]),
            (_, result) => console.log("INSERT failed:" + result);
        }
      );
    });
  };

  const removeFromTeamMembers = async (item, id) => {
    setListOfAddedTeamMembers(
      listOfAddedTeamMembers.filter((item) => item.id !== id)
    );
    setListOfTeamMembers([...listOfTeamMembers, item]);
  };

  const addToAddedTeamMembers = async (item, id) => {
    setListOfAddedTeamMembers([...listOfAddedTeamMembers, item]);
    setListOfTeamMembers(listOfTeamMembers.filter((item) => item.id !== id));
    list2[count2] = id;
    count2++;
    setListOfTeamDBIndecies(list2);
    await setExists(true);
  };

  return (
    <View style={styles.background}>
      <Text style={styles.headerText}>Add Project</Text>
      <ScrollView
        contentContainerStyle={styles.scrollView}
        style={styles.container}
      >
        <View style={styles.content}>
          <Text style={styles.textStyle}>Project Name</Text>
          <TextInput
            placeholder={"project"}
            value={name}
            onChangeText={handleProject}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Team Name</Text>
          <TextInput
            placeholder={"team name"}
            value={team}
            onChangeText={handleTeam}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Project Start Date</Text>
          <TextInput
            value={date}
            onChangeText={formatDate}
            placeholder={"date of project"}
            style={styles.textBoxStyle}
          />
          <Text style={styles.textStyle}>Hours Per Point</Text>
          <TextInput
            keyboardType="numeric"
            value={hours.toString()}
            onChangeText={handleHours}
            placeholder={"hours per point"}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Estimated Story Points</Text>
          <TextInput
            keyboardType="numeric"
            value={story.toString()}
            placeholder={"estimated story points"}
            onChangeText={handleEstimate}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Estimated Cost</Text>
          <TextInput
            keyboardType="numeric"
            value={cost.toString()}
            placeholder={"estimated cost"}
            onChangeText={handleCost}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Velocity</Text>
          <TextInput
            keyboardType="numeric"
            value={velocity.toString()}
            placeholder={"velocity"}
            onChangeText={handleVelocity}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Description</Text>
          <TextInput
            placeholder={"description"}
            value={description}
            onChangeText={handleDescription}
            style={styles.textBoxStyle}
          />
          <Text style={styles.textStyle}>Added Team Members</Text>
            {listOfAddedTeamMembers.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  onPress={() => removeFromTeamMembers(item, item.id)}
                  style={styles.textBoxStyle}>
                  <View key={index}>
                    <Text style={styles.textStyle}>{item.name}</Text>
                  </View>
                </TouchableOpacity>
              );
            })}
          <Text style={styles.textStyle}>List of Team Members</Text>
          {listOfTeamMembers.map((item, index) => {
            return (
              <TouchableOpacity
                style={styles.textBoxStyle}
                key={index}
                onPress={() => addToAddedTeamMembers(item, item.id)}
              >
                <View key={index}>
                  <Text style={styles.textStyle}>{item.name}</Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>
      <View style={{flexDirection: "row", justifyContent:"space-evenly", padding: 5, margin: 5}}>
      <View style={styles.buttonWidth}>
        <TouchableOpacity
          style={styles.buttons}
          onPress={() => {
            insertData();
          }}
        >
          <Text style={styles.buttonText}>Add Project</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.buttonWidth}>
        <TouchableOpacity
          style={styles.buttons}
          onPress={() => {
            navigation.navigate("AddProductBacklog");
          }}
        >
          <Text style={styles.buttonText}>Add Subtask</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.buttonWidth}>
        <TouchableOpacity
          style={styles.buttons}
          onPress={() => {
            navigation.navigate("Home");
          }}
        >
          <Text style={styles.buttonText}>Return</Text>
        </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

export default addNewProject;
