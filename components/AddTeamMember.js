import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
  Alert,
} from "react-native";
import db from "./Database";
import styles from "../styles/member_add_style.js";
import { useNavigation} from "@react-navigation/native";

function addNewTeamMember(props) {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  const navigation = useNavigation();
  const handleFirstName = (name) => {
    setFirstName(name);
  };

  const handleLastName = (name) => {
    setLastName(name);
  };

  const insertData = async () => {
    db.transaction((tx) => {
      tx.executeSql(
        `INSERT INTO Team_Member (first_name, last_name) values (?, ?)`,
        [firstName, lastName],
        (_, { rowsAffected }) =>
          rowsAffected > 0
            ? Alert.alert("Alert!", `Entry added`, [
                { text: "close", onPress: () => navigation.navigate("Home") },
              ])
            : Alert.alert("Alert!", `Entry failed to add`, [{ text: "close" }]),
        (_, result) => console.log("INSERT failed:" + result)
      );
    });
  };

  const onClick = () => {
    if (
      firstName == "" ||
      firstName == undefined ||
      lastName == "" ||
      lastName == undefined
    ) {
      Alert.alert("Alert!", `Missing name(s).`, [{ text: "close" }]);
      return;
    }
    insertData();
  };

  return (
    <View style={styles.background}>
      <Text style={styles.headerText}>Add Team Member</Text>
      <View style={styles.container}>
        <View style={styles.content}>
          <Text style={styles.textStyle}>First Name</Text>
          <TextInput
            placeholder={"first name"}
            value={firstName}
            onChangeText={handleFirstName}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Last Name</Text>
          <TextInput
            placeholder={"last name"}
            value={lastName}
            onChangeText={handleLastName}
            style={styles.textBoxStyle}
          />
        </View>
      </View>

      <View style={styles.buttonWidth}>
        <TouchableOpacity style={styles.buttons} onPress={onClick}>
          <Text style={styles.buttonText}>Add</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.buttonWidth}>
        <TouchableOpacity
          style={styles.buttons}
          onPress={() => {
            navigation.navigate("Home");
          }}
        >
          <Text style={styles.buttonText}>Return</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default addNewTeamMember;
