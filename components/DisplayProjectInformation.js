import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
} from "react-native";
import styles from "../styles/project_info_style.js";
import db from "./Database";
import { useNavigation } from "@react-navigation/native";
import * as Print from "expo-print";
import * as FileSystem from "expo-file-system";
import * as IntentLauncher from "expo-intent-launcher";
import * as Permissions from "expo-permissions";
import * as MediaLibrary from "expo-media-library";

function DisplayProjectInformation(props) {
  const [id, setID] = useState(props.projDBIndex);
  const [entryIndex, setEntryIndex] = useState(props.projIndex);
  const [show, setShow] = useState(false);
  const [mode, setMode] = useState("date");
  const [date, setDate] = useState("");
  const [name, setName] = useState("");
  const [team, setTeam] = useState("");
  const [hours, setHours] = useState(0);
  const [story, setStory] = useState(0);
  const [cost, setCost] = useState(0);
  const [velocity, setVelocity] = useState(0);
  const [description, setDescription] = useState("");
  const [error, setError] = useState("");
  const [listOfTeamMembers, setListOfTeamMembers] = useState([]);
  const [listOfIndecies, setListOfIndecies] = useState([]);
  const [backlogBtnText, setBacklogBtnText] = useState("Add");
  const [PDFHTML, setPDFHTML] = useState("");
  const [totalHours, setTotalHours] = useState(0);
  const [totalReestimate, setTotalReestimate] = useState(0);
  const [percentTotal, setPercentTotal] = useState(0);
  const [originalTotal, setOriginalTotal] = useState(0);
  const navigation = useNavigation();
  useEffect(() => {
    loadData();
    loadTeamMembers();
    loadProductBacklog();
  }, []);
  const setDateChosen = (e, selectedValue) => {
    setDate(selectedValue);
    setShow(false);
  };

  const setTeamMembers = (list) => {
    setListOfTeamMembers(list);
  };

  const setHTML = (html) => {
    setPDFHTML(html);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatePicker = () => {
    showMode("date");
  };

  const formatDate = (date) => {
    setDate(date);
  };

  const handleProject = (name) => {
    setName(name);
  };

  const handleTeam = (team) => {
    setTeam(team);
  };

  const handleHours = (hours) => {
    let number = parseInt(hours);
    setHours(number);
  };

  const handleEstimate = (story) => {
    let number = parseInt(story);
    setStory(number);
  };

  const handleCost = (cost) => {
    let number = parseInt(cost);
    setCost(number);
  };

  const handleVelocity = (velocity) => {
    let number = parseInt(velocity);
    setVelocity(number);
  };

  const handleDescription = (description) => {
    setDescription(description);
  };

  const goBack = async () => {
    navigation.navigate("Home");
  };

  const handleProductBacklog = async () => {
    if (backlogBtnText === "Add") {
      navigation.navigate("AddProductBacklog");
    } else {
      navigation.navigate("DisplayProjectBacklog");
    }
  };

  const deleteData = async () => {
    db.transaction((tx) => {
      tx.executeSql(
        `DELETE FROM Projects WHERE projects_id = ${props.projDBIndex}`,
        [],
        (_, { rowsAffected }) =>
          rowsAffected > 0
            ? console.log("ROW DELETED!")
            : console.log("DELETE FAILED!"),
        (_, result) => console.log("DELETE failed:" + result)
      );
    });

    navigation.navigate("Home");
  };
  const updateData = async () => {
    db.transaction((tx) => {
      // executeSql(sqlStatement, arguments, success, error)
      tx.executeSql(
        `UPDATE Projects SET team_name = ? , project_name = ? , project_start_date = ? , hours_equivalent_story_point = ? , estimated_story_points = ? , estimated_cost = ? , velocity = ? , project_description = ? WHERE projects_id = ${props.projDBIndex}`,
        [team, name, date, hours, story, cost, velocity, description],
        (_, { rowsAffected }) =>
          rowsAffected > 0
            ? console.log("ROW UPDATED!")
            : console.log("UPDATE FAILED!"),
        (_, result) => console.log("UPDATE failed:" + result)
      );
    });
    Alert.alert("Alert!", `Project has been updated.`, [{ text: "close" }]);
  };

  const loadData = async () => {
    let id2 = 1;
    let id3 = 1;
    let count = 1;
    let count2 = 0;
    let count3 = 0;
    let count4 = 0;
    let actualHoursTotal = [];
    let restimateHoursTotal = [];
    let percentCompleteTotal = [];
    let originalEstimateTotal = [];
    let listOfIds = [];
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM Product_Backlog WHERE project_id = ${props.projDBIndex}`,
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          let entries = rows._array;
          if (entries > 0) {
            id2 = entries[0].product_backlog_id;
          }
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM User_Stories WHERE project_backlog_id = ${id2}`,
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          let entries3 = rows._array;
          for (let i = 0; i < entries3.length; ++i) {
            percentCompleteTotal[i] = 0;
            originalEstimateTotal[i] = 0;
            listOfIds[i] = 0;
          }
          entries3.forEach((entry) => {
            percentCompleteTotal[entry.user_story_id - 1] += parseInt(
              entry.percent_complete
            );
            originalEstimateTotal[entry.user_story_id - 1] += parseInt(
              entry.original_estimate
            );
            listOfIds[count3] = entry.user_story_id;
            count3++;
            let pT = 0;
            let oT = 0;
            for (let i = 0; i < percentCompleteTotal.length; ++i) {
              pT += percentCompleteTotal[i];
              oT += originalEstimateTotal[i];
            }
            pT /= percentCompleteTotal.length;
            setPercentTotal(pT);
            setOriginalTotal(oT);
          });
          for(let i =0; i < listOfIds.length; ++i)
          {
            db.transaction((tx) => {
              tx.executeSql(
                `SELECT * FROM Subtasks WHERE user_story_id = ${listOfIds[i]}`,
                [],
                (_, { rows }) => {
                  console.log("ROWS RETRIEVED!");
                  let entries2 = rows._array;
                  if(i == 0)
                  {
                  for (let i = 0; i < listOfIds.length; ++i) {
                    actualHoursTotal[i] = 0;
                    restimateHoursTotal[i] = 0;
                  }
                }
                  entries2.forEach((entry2) => {
                    console.log(entry2);
                    actualHoursTotal[entry2.subtask_id - 1] += 
                      parseInt(entry2.hours_worked);
                    
                    restimateHoursTotal[entry2.subtask_id - 1] += 
                      parseInt(entry2.remaining_hours);
                    let tH = 0;
                    let rH = 0;
                    for (let i = 0; i < actualHoursTotal.length; ++i) {
                    for(let j = 0; j < actualHoursTotal.length; ++j)
                    {
                      if(isNaN(actualHoursTotal[j]) || actualHoursTotal[j] == undefined)
                      {
                        actualHoursTotal.splice(j, 1);
                        break;
                      }
                      
                    }
                    }
                    for(let i = 0; i < actualHoursTotal.length; ++i)
                    {
                      tH += actualHoursTotal[i];
                      rH += restimateHoursTotal[i];
                    }
                    console.log(actualHoursTotal);
                    setTotalHours(tH);
                    setTotalReestimate(rH);
                  });
                },
                (_, result) => {
                  console.log("SELECT failed!");
                  console.log(result);
                }
              );
            });
          }
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });
    let hold = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Pdf Content</title>`;
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM Projects`,
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          let entries = rows._array;
          hold +=
            "<h1>Team Name: " + entries[entryIndex - 1].team_name + "</h1>";
          setTeam(entries[entryIndex - 1].team_name);
          setName(entries[entryIndex - 1].project_name);
          setDate(entries[entryIndex - 1].project_start_date);
          setVelocity(entries[entryIndex - 1].velocity + "");
          setDescription(entries[entryIndex - 1].project_description);
          setHours(entries[entryIndex - 1].hours_equivalent_story_point + "");
          setStory(entries[entryIndex - 1].estimated_story_points + "");
          setCost(entries[entryIndex - 1].estimated_cost + "");
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM Product_Backlog WHERE project_id = ${props.projDBIndex}`,
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          let entries = rows._array;
          if (entries.length > 0) {
            id2 = entries[0].product_backlog_id;
          }
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM User_Stories WHERE project_backlog_id = ${id2}`,
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          let entries3 = rows._array;
          entries3.forEach((entry) => {
            db.transaction((tx) => {
              tx.executeSql(
                `SELECT * FROM Subtasks WHERE user_story_id = ${entry.user_story_id}`,
                [],
                (_, { rows }) => {
                  console.log("ROWS RETRIEVED!");
                  let entries2 = rows._array;
                  entries2.forEach((entry2) => {
                    if (count == 1) {
                      hold += "<h3>Sprint: " + entries2[0].sprint_num + "</h3>";
                      hold +=
                        '<table style="border: 1px solid black; border-collapse: collapse">';
                      hold += "<tr>";
                      hold +=
                        '<th style="border: 1px solid black;">User Stories/Sub Tasks</th>';
                      hold +=
                        '<th style="border: 1px solid black;">Team Member</th>';
                      hold +=
                        '<th style="border: 1px solid black;">Percentage Complete</th>';
                      hold +=
                        '<th style="border: 1px solid black;">Original Hours Est.</th>';
                      hold +=
                        '<th style="border: 1px solid black;">Actual Hours Worked</th>';
                      hold +=
                        '<th style="border: 1px solid black;">Re-Estimate to Complete</th>';
                      hold += "</tr>";
                      count++;
                    }
                    if (count2 == 0) {
                      hold += "<tr>";
                      hold +=
                        '<td style="text-align:center; border: 1px solid black; font-weight:bold">' +
                        entry.user_story_text +
                        "</td>";
                      hold +=
                        '<td style="text-align:center; border: 1px solid black;"></td>';
                      hold +=
                        '<td style="text-align:center; border: 1px solid black;">' +
                        percentCompleteTotal[entry.user_story_id - 1] +
                        "%</td>";
                      hold +=
                        '<td style="text-align:center; border: 1px solid black;">' +
                        originalEstimateTotal[entry.user_story_id - 1] +
                        "</td>";
                      hold +=
                        '<td style="text-align:center; border: 1px solid black;">' +
                        actualHoursTotal[entry2.subtask_id - 1] +
                        "</td>";
                      hold +=
                        '<td style="text-align:center; border: 1px solid black;">' +
                        restimateHoursTotal[entry2.subtask_id - 1] +
                        "</td>";
                      hold += "</tr>";
                      count2++;
                    }
                    hold += "\n<tr>";
                    hold +=
                      '\n<td style="text-align:center; border: 1px solid black;">' +
                      entry2.name +
                      "</td>";
                    hold +=
                      '\n<td style="text-align:center; border: 1px solid black;">' +
                      entry2.team_member_name +
                      "</td>";
                    hold +=
                      '\n<td style="text-align:center; border: 1px solid black;"></td>';
                    hold +=
                      '\n<td style="text-align:center; border: 1px solid black;"></td>';
                    hold +=
                      '\n<td style="text-align:center; border: 1px solid black;">' +
                      entry2.hours_worked +
                      "</td>";
                    hold +=
                      '\n<td style="text-align:center; border: 1px solid black;">' +
                      entry2.remaining_hours +
                      "</td>";
                    hold += "\n</tr>";
                  });
                  count2 = 0;
                  hold += "\n<tr>";
                  hold += '\n<td style="padding: 10px"></td>';
                  hold += "\n<td></td>";
                  hold += "\n<td></td>";
                  hold += "\n<td></td>";
                  hold += "\n<td></td>";
                  hold += "\n<td></td>";
                  hold += "\n</tr>";
                  setHTML(hold);
                },
                (_, result) => {
                  console.log("SELECT failed!");
                  console.log(result);
                }
              );
            });
          });
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });
  };

  const loadTeamMembers = async () => {
    let list = [];
    let list2 = [];
    db.transaction((tx) => {
      tx.executeSql(
        "SELECT * FROM Projects_Team_Members",
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          let entries = rows._array;

          let count = 0;
          entries.forEach((entry) => {
            if (entry.project_id === props.projDBIndex) {
              list[count] = entry.team_member_id;
              count++;
            }
          });
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });

    db.transaction((tx) => {
      tx.executeSql(
        "SELECT * FROM Team_Member",
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          let entries = rows._array;

          let count = 0;
          entries.forEach((entry) => {
            if (entry.team_member_id == list[count]) {
              list2[count] = entry.first_name + " " + entry.last_name;
              count++;
            }
          });
          setTeamMembers(list2);
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });
  };

  const loadProductBacklog = async () => {
    let list = [];
    let list2 = [];
    db.transaction((tx) => {
      tx.executeSql(
        "SELECT * FROM Product_Backlog",
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          let entries = rows._array;

          let count = 0;
          entries.forEach((entry) => {
            if (entry.project_id === props.projDBIndex) {
              setBacklogBtnText("View");
            }
          });
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });
  };

  const generatePDF = async () => {
    let html = PDFHTML;
    html += "<tr>";
    html +=
      '\n<td style="text-align:left; border: 1px solid black;">Total</td>';
    html += '\n<td style="text-align:center; border: 1px solid black;"></td>';
    html +=
      '\n<td style="text-align:center; border: 1px solid black;">' +
      Math.round(percentTotal) +
      "%</td>";
    html +=
      '\n<td style="text-align:center; border: 1px solid black;">' +
      originalTotal +
      "</td>";
    html +=
      '\n<td style="text-align:center; border: 1px solid black;">' +
      totalHours +
      "</td>";
    html +=
      '\n<td style="text-align:center; border: 1px solid black;">' +
      totalReestimate +
      "</td>";
    html += "</tr>";
    html += "</table>";
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status == "granted") {
      const { uri } = await Print.printToFileAsync({ html });
      const asset = await MediaLibrary.createAssetAsync(uri);
      await MediaLibrary.createAlbumAsync("Download", asset, false);
    }
    Alert.alert("Alert!", `PDF file generated. Check downloads folder to access file.`, [{ text: "close" }]);
  };

  return (
    <View style={styles.background}>
      <Text style={styles.headerText}>Project #{entryIndex} Information</Text>
      <Text>{error}</Text>
      <ScrollView
        contentContainerStyle={styles.scrollView}
        style={styles.container}
      >
        <View style={styles.content}>
          <Text style={styles.textStyle}>Project Name</Text>
          <TextInput
            placeholder={"project"}
            value={name.toString()}
            onChangeText={handleProject}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Team Name</Text>
          <TextInput
            placeholder={"team name"}
            value={team}
            onChangeText={handleTeam}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Project Start Date</Text>
          <TextInput
            value={date}
            onChangeText={formatDate}
            placeholder={"date of project"}
            style={styles.textBoxStyle}
          />
          <Text style={styles.textStyle}>Hours Per Point</Text>
          <TextInput
            keyboardType="numeric"
            value={hours.toString()}
            onChangeText={handleHours}
            placeholder={"hours per point"}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Estimated Story Points</Text>
          <TextInput
            keyboardType="numeric"
            value={story.toString()}
            placeholder={"estimated story points"}
            onChangeText={handleEstimate}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Estimated Cost</Text>
          <TextInput
            keyboardType="numeric"
            value={cost.toString()}
            placeholder={"estimated cost"}
            onChangeText={handleCost}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Velocity</Text>
          <TextInput
            keyboardType="numeric"
            value={velocity.toString()}
            placeholder={"velocity"}
            onChangeText={handleVelocity}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>Description</Text>
          <TextInput
            placeholder={"description"}
            value={description}
            onChangeText={handleDescription}
            style={styles.textBoxStyle}
          />
          <Text>Team Members:</Text>
          {listOfTeamMembers.map((item, index) => {
            return (
              <TouchableOpacity key={index}>
                <View></View>
                <Text style={styles.textBoxStyle}>
                  {item}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>
      <View style={styles.buttonWidth}>
      <View style={{flexDirection: "row", justifyContent:"space-evenly", padding: 5, margin: 5}}>
        <TouchableOpacity style={styles.buttons} onPress={generatePDF}>
          <Text style={styles.buttonText}>Generate PDF</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttons} onPress={handleProductBacklog}>
          <Text style={styles.buttonText}>{backlogBtnText}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttons} onPress={updateData}>
          <Text style={styles.buttonText}>Update</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttons} onPress={deleteData}>
          <Text style={styles.buttonText}>Delete</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttons} onPress={goBack}>
          <Text style={styles.buttonText}>Return</Text>
        </TouchableOpacity>
      </View>
      </View>
    </View>
  );
}

export default DisplayProjectInformation;
