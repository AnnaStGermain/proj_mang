import React, {useState, useEffect } from "react";
import {View} from 'react-native';
import db from './Database';

function LoadProjectInformation(props) {
  const {
    listOfProjectNames
  } = props;

    useEffect(
        () => {
            db.transaction((tx) => {
                tx.executeSql(
                  "SELECT * FROM Projects",
                  [],
                  (_, { rows }) => {
                    console.log("ROWS RETRIEVED!");
          
                    // clear data currently stored
                      if(rows._array !== undefined)
                      {                       
                        let entries = rows._array;
                        
                        let list = [];
                        let count = 0;
                        entries.forEach((entry) => {
                          list[count] = entry.project_name;
                          count++;
                      });
                      listOfProjectNames(list);
                    }
                  },
                  (_, result) => {
                    console.log("SELECT failed!");
                    console.log(result);
                  }
                );
              });
        },
        []
      );
      return (
        <View></View>
      );
};

export default LoadProjectInformation;