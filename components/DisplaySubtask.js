import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import {
    Text,
    View,
    Button,
    TextInput,
    TouchableOpacity,
    Alert,
    ScrollView
} from "react-native";
import db from "./Database";
import { useNavigation } from '@react-navigation/native';
import styles from "../styles/display_subtask";
let passedID = 1;
function checkNumeric(str) {
    if (typeof (str) !== "string") return false;
    return !isNaN(str) && !isNaN(parseFloat(str))
}

function DisplaySubTask(props) {
    const [id, setID] = useState();
    const [entryIndex, setEntryIndex] = useState(props.projIndex);
    const [userStoryText, setUserStoryText] = useState("");
    const [description, setDescription] = useState("");
    const [hours_worked, setHoursWorked] = useState("");
    const [hours_Remaining, setHoursRemainingd] = useState("");
    const [sprint, setSprint] = useState("0");
    const [userStories, setUserStories] = useState([]);
    const navigation = useNavigation();
    useEffect(() => {
        loadData();
    }, []);

    const goBack = async () => {
        navigation.navigate("Home");
    };

    const handleuserStoryText = (newStory) => {
        setUserStoryText(newStory);
    };

    const handleDescription = (text) => {
        setDescription(text);
    };

    const handleHoursWorked = (text) => {
        //let number = parseInt(text);
        setHoursWorked(text);
    };

    const handleHoursRemaining = (text) => {
        //let number = parseInt(text);
        setHoursRemainingd(text);
    };

    const handleSprint = (text) => {
      //  let number = parseInt(text);
        setSprint(text);
    };



    const deleteData = async () => {
        db.transaction((tx) => {
            tx.executeSql(
                `DELETE FROM Subtasks WHERE subtask_id = ${passedID}`,
                [],
                (_, { rowsAffected }) =>
                    rowsAffected > 0
                        ? console.log("ROW DELETED!")
                        : console.log("DELETE FAILED!"),
                (_, result) => console.log("DELETE failed:" + result)
            );
        });

        navigation.navigate("Home");
    };

    const updateData = async () => {
        db.transaction((tx) => {
            // executeSql(sqlStatement, arguments, success, error)
            tx.executeSql(
                `UPDATE Subtasks SET name = ?, description = ?, hours_worked = ?, remaining_hours = ?, sprint_num = ? WHERE subtask_id = ${passedID}`,
                [userStoryText, description, hours_worked, hours_Remaining, sprint],
                (_, { rowsAffected }) =>
                    rowsAffected > 0
                        ? console.log("ROW UPDATED!")
                        : console.log("UPDATE FAILED!"),
                (_, result) => console.log("UPDATE failed:" + result)
            );
        });

      //  reInsertSubtasks(passedID)

    };



    const loadData = async () => {
        db.transaction((tx) => {
            tx.executeSql(
                `SELECT * FROM Subtasks WHERE subtask_id = ${passedID}`,
                [],
                (_, { rows }) => {
                    console.log("ROWS RETRIEVED!");

                    let entries = rows._array;
                    handleuserStoryText(entries[0].name);
                    handleDescription(entries[0].description)
                    handleHoursWorked(entries[0].hours_worked + "")
                    handleHoursRemaining(entries[0].remaining_hours + "")
                    handleSprint(entries[0].sprint_num + "")
                },
                (_, result) => {
                    console.log("SELECT failed!");
                    console.log(result);
                }
            );
        });
    };

    const viewUserStory = async (id) => {
        props.handleUserpassedID(userStories[id])
        navigation.navigate("AddProject");
    };

    const addSubtask = () => {
        if (userStoryText !== "" || userStoryText !== undefined) {
            reInsertSubtasks(passedID)
            var newArr = subTasks.concat(userStoryText);
            console.log(newArr);
            setSubTasks(newArr);
            Alert.alert("New SubTask Added!");
        }
    }

    return (
        <View style={styles.content}>
            <Text style={styles.headerText}>SubTask Information</Text>
            <Text style={styles.textStyle}>Subtask Name: </Text>
            <TextInput
                placeholder={"userStoryText"}
                value={userStoryText}
                onChangeText={handleuserStoryText}
                style={styles.textBoxStyle}
            />
            <Text style={styles.textStyle}>Description: </Text>
            <TextInput
                placeholder={"description"}
                value={description}
                onChangeText={handleDescription}
                style={styles.textBoxStyle}
            />
            <Text style={styles.textStyle}>Hours Worked: </Text>
            <TextInput
                placeholder={"hours_worked"}
                value={hours_worked}
                onChangeText={handleHoursWorked}
                style={styles.textBoxStyle}
            />
            <Text style={styles.textStyle}>Hours Remaining: </Text>
            <TextInput
                placeholder={"hours_Remaining"}
                value={hours_Remaining}
                onChangeText={handleHoursRemaining}
                style={styles.textBoxStyle}
            />
            <Text style={styles.textStyle}>Sprint # (To Be completed in): </Text>
            <TextInput
                placeholder={"sprint"}
                value={sprint}
                onChangeText={handleSprint}
                style={styles.textBoxStyle}
            />
            <View style={{flexDirection: "row", justifyContent:"space-evenly"}}>
            <TouchableOpacity style={styles.buttons} onPress={updateData}><Text style={styles.buttonText}>Update</Text></TouchableOpacity>
            <TouchableOpacity style={styles.buttons} onPress={deleteData}><Text style={styles.buttonText}>Delete</Text></TouchableOpacity>
            <TouchableOpacity style={styles.buttons} onPress={goBack}><Text style={styles.buttonText}>Go Back</Text></TouchableOpacity>
            </View>
        </View>
    );
}


export function PassSubIndex(props) {
    passedID = props.storyIDs;
    const navigation = useNavigation();
    const passID = () => {
        passedID = props.storyIDs;
        navigation.navigate("DisplaySubtask");
        console.log("ID: " + passedID);
    }

    return (<View style={styles.buttonWidth}>
        <TouchableOpacity style={styles.buttons} onPress={() => passID()}><Text style={styles.buttonText}>Load Subtask {passedID}</Text></TouchableOpacity>
    </View>);
};
export default DisplaySubTask;
