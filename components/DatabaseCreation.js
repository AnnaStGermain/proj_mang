import React, { useState, useEffect } from "react";
import { View } from 'react-native';
import * as SQLite from "expo-sqlite";
import db from './Database';

const DatabaseCreation = () => {
  useEffect(
    () => {
      db.transaction((tx) => {
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS Retrospective (retrospective_id  INTEGER PRIMARY KEY NOT NULL, subtask_id INTEGER NOT NULL, FOREIGN KEY(subtask_id) REFERENCES substasks(subtask_id));",
          [],
          () => console.log("Retrospective Table CREATED!"),
          (_, result) => console.log("TABLE CREATE failed:" + result)
        );
      });
      db.transaction((tx) => {
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS Subtasks (subtask_id INTEGER PRIMARY KEY NOT NULL, user_story_id INTEGER NOT NULL, team_member_name TEXT NOT NULL, name TEXT NOT NULL, description TEXT NOT NULL, hours_worked INTEGER NOT NULL, remaining_hours INTEGER NOT NULL, sprint_num INTEGER NOT NULL, FOREIGN KEY(user_story_id) REFERENCES User_Stories(user_story_id));",
          [],
          () => console.log("Subtasks Table CREATED!"),
          (_, result) => console.log("TABLE CREATE failed:" + result)
        );
      });
      db.transaction((tx) => {
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS Projects (projects_id INTEGER PRIMARY KEY NOT NULL, team_name TEXT NOT NULL, project_name TEXT NOT NULL, project_start_date TEXT NOT NULL, hours_equivalent_story_point INTEGER NOT NULL, estimated_story_points INTEGER NOT NULL, estimated_cost INTEGER NOT NULL, velocity INTEGER NOT NULL, project_description TEXT NOT NULL);",
          [],
          () => console.log("Projects Table CREATED!"),
          (_, result) => console.log("TABLE CREATE failed:" + result)
        );
      });
      db.transaction((tx) => {
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS Team_Member (team_member_id INTEGER PRIMARY KEY NOT NULL, first_name TEXT NOT NULL, last_name TEXT NOT NULL);",
          [],
          () => console.log("Team_Member Table CREATED!"),
          (_, result) => console.log("TABLE CREATE failed:" + result)
        );
      });
      db.transaction((tx) => {
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS Projects_Team_Members (projects_team_members_id INTEGER PRIMARY KEY NOT NULL, project_id INTEGER NOT NULL, team_member_id INTEGER NOT NULL, FOREIGN KEY (project_id) REFERENCES projects(projects_id), FOREIGN KEY (team_member_id) REFERENCES team_member (team_member_id));",
          [],
          () => console.log("Projects_Team_Members Table CREATED!"),
          (_, result) => console.log("TABLE CREATE failed:" + result)
        );
      });
      db.transaction((tx) => {
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS User_Stories (user_story_id INTEGER PRIMARY KEY NOT NULL, project_backlog_id INTEGER NOT NULL, percent_complete TEXT NOT NULL, original_estimate TEXT NOT NULL, user_story_text TEXT NOT NULL, FOREIGN KEY (project_backlog_id) REFERENCES Product_Backlog(product_backlog_id));",
          [],
          () => console.log("User_Stories Table CREATED!"),
          (_, result) => console.log("TABLE CREATE failed:" + result)
        );
      });
      db.transaction((tx) => {
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS Product_Backlog (product_backlog_id INTEGER PRIMARY KEY NOT NULL, initial_relative_estimate INTEGER NOT NULL, inital_estimated_cost INTEGER NOT NULL, project_id INTEGER NOT NULL);",
          [],
          () => console.log("Product_Backlog Table CREATED!"),
          (_, result) => console.log("TABLE CREATE failed:" + result)
        );
      });
    },
    []
  );
  return (
    <View></View>
  );
};

export default DatabaseCreation;