import { useNavigation } from "@react-navigation/native";
import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
} from "react-native";
import db from "./Database";
import styles from "../styles/subtask_add_style.js";

function checkNumeric(str) {
  if (typeof str !== "string") return false;
  return !isNaN(str) && !isNaN(parseFloat(str));
}

function addNewProjectBackLog(props) {
  const [id, setID] = useState(props.projDBIndex);
  const [relativeEstimate, setRelativeEstimate] = useState("");
  const [estimatedTotal, setEstimatedTotal] = useState("");
  const [userStories, setUserStories] = useState([""]);
  const [newUserStory, setNewUserStory] = useState("");
  const [percentComplete, setPercent] = useState("");
  const [originalEstimate, setOriginalEstimate] = useState("");

  const navigation = useNavigation();

  const handleRelativeEstimate = (relativeEstimate) => {
    if (checkNumeric(relativeEstimate)) setRelativeEstimate(relativeEstimate);
    else Alert.alert(`ERROR!`, "Relative Cost must be a number...");
  };

  const handleEstimatedTotal = (estimated) => {
    if (checkNumeric(estimated)) setEstimatedTotal(estimated);
    else Alert.alert(`ERROR!`, "Estimated Total must be a number...");
  };

  const handleNewUserStory = (newStory) => {
    setNewUserStory(newStory);
  };

  const handleNewUserStory2 = (newStory) => {
    setPercent(newStory);
  };

  const handleNewUserStory3 = (newStory) => {
    setOriginalEstimate(newStory);
  };

  const insertData = async () => {
    console.log(`user stories length: ${userStories.length}`);
    var lastInsertId = 0;
    if (userStories.length > 1) {
      db.transaction((tx) => {
        tx.executeSql(
          `INSERT INTO Product_Backlog (initial_relative_estimate, inital_estimated_cost, project_id) values (?,?,?)`,
          [relativeEstimate, estimatedTotal, id],
          function (tx, results) {
            insertUserStories(results.insertId); // this is the id of the insert just performed
          },
          (_, result) => console.log("INSERT failed:" + result)
        );
      });
    } else
      Alert.alert(
        "Missing Data!",
        "User Stories not entered, please enter atlease one before continuing..."
      );
  };

  const insertUserStories = async (id) => {
    console.log(`id inserted ${id}`);
    userStories.forEach((item) => {
      if (item != "") {
        db.transaction((tx) => {
          tx.executeSql(
            `INSERT INTO User_Stories (project_backlog_id, user_story_text, percent_complete, original_estimate) values (?,?,?,?)`,
            [id, item, percentComplete, originalEstimate],
            (_, { rowsAffected }) =>
              rowsAffected > 0
                ? Alert.alert("Alert!", `User story added`, [
                    {
                      text: "close",
                      onPress: () => navigation.navigate("Home"),
                    },
                  ])
                : Alert.alert("Alert!", `User story failed to add`, [
                    { text: "close" },
                  ]),
            (_, result) => console.log("INSERT failed:" + result)
          );
        });
      }
    });
    Alert.alert("Entry Added!");
  };

  const onClick = () => {
    insertData();
  };

  const addUserStory = () => {
    if (newUserStory !== "" || newUserStory !== undefined) {
      var newArr = userStories.concat(newUserStory);
      setUserStories(newArr);
    }
  };

  return (
    <View>
      <Text style={styles.headerText}>Add Product Backlog</Text>
      <ScrollView
        contentContainerStyle={styles.scrollView}
        style={styles.container}
      >
        <View style={styles.content}>
          <Text style={styles.textStyle}>Inital Relative Estimate</Text>
          <TextInput
            placeholder={"relativeEstimate"}
            value={relativeEstimate}
            onChangeText={handleRelativeEstimate}
            style={styles.textBoxStyle}
          />
          <Text style={styles.textStyle}>Inital Estimated Total Cost</Text>
          <TextInput
            placeholder={"estimatedTotal"}
            value={estimatedTotal}
            onChangeText={handleEstimatedTotal}
            style={styles.textBoxStyle}
          />

          <Text style={styles.textStyle}>List Of User Stories</Text>
          {userStories.map((item, index) => {
            return (
              <TouchableOpacity key={index}>
                <View key={index}>
                  <Text>{item}</Text>
                </View>
              </TouchableOpacity>
            );
          })}
          <Text style={styles.textStyle}>Enter New User Story</Text>
          <TextInput
            placeholder={"User Story Name"}
            value={newUserStory}
            onChangeText={handleNewUserStory}
            style={styles.textBoxStyle}
          />
          <TextInput
            placeholder={"User Percent Complete"}
            value={percentComplete}
            onChangeText={handleNewUserStory2}
            style={styles.textBoxStyle}
          />
          <TextInput
            placeholder={"User Story Original Hours"}
            value={originalEstimate}
            onChangeText={handleNewUserStory3}
            style={styles.textBoxStyle}
          />
        </View>
      </ScrollView>

      <View style={{flexDirection: "row", justifyContent:"space-evenly"}}>
        <TouchableOpacity style={styles.buttons} onPress={addUserStory}>
          <Text style={styles.buttonText}>Add User Story</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttons} onPress={onClick}>
          <Text style={styles.buttonText}>Add Product Backlog</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttons}
          onPress={() => {
            navigation.navigate("Home");
          }}
        >
          <Text style={styles.buttonText}>Return</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default addNewProjectBackLog;
