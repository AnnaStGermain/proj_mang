import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import { Text, View, TouchableOpacity, ScrollView } from "react-native";
import db from "./Database";
import { useNavigation } from "@react-navigation/native";
import styles from "../styles/view_members_style.js";

function addNewTeamMember(props) {
  const [listOfTeamMembers, setListOfTeamMembers] = useState([]);
  const [listOfTeamMemberIds, setListOfTeamMemberIds] = useState([]);
  const navigation = useNavigation();

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    db.transaction((tx) => {
      tx.executeSql(
        "SELECT * FROM Team_Member",
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          // clear data currently stored
          if (rows._array !== undefined) {
            let entries = rows._array;

            let list = [];
            let list2 = [];
            let count = 0;
            entries.forEach((entry) => {
              list[count] = entry.first_name + " " + entry.last_name;
              list2[count] = entry.team_member_id;
              count++;
            });
            setListOfTeamMembers(list);
            setListOfTeamMemberIds(list2);
          }
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });
  };

  const deleteData = async (id) => {
    db.transaction((tx) => {
      tx.executeSql(
        `DELETE FROM Team_Member WHERE team_member_id = ${listOfTeamMemberIds[id]}`,
        [],
        (_, { rowsAffected }) =>
          rowsAffected > 0
            ? console.log("ROW DELETED!")
            : console.log("DELETE FAILED!"),
        (_, result) => console.log("DELETE failed:" + result)
      );
    });
    loadData();
  };

  return (
    <View style={styles.background}>
      <Text style={styles.headerText}>Team Members</Text>
      <ScrollView
        contentContainerStyle={styles.scrollView}
        style={styles.container}
      >
        {listOfTeamMembers.map((item, index) => {
          return (
            <View style={styles.textBoxStyle}>
              <Text style={styles.textStyle}>{item}</Text>
              <TouchableOpacity style={styles.styleDelete} key={index} onPress={() => deleteData(index)}>
                <View key={index}>
                  <Text style={styles.textStyle}>x</Text>
                </View>
              </TouchableOpacity>
            </View>
          );
        })}
      </ScrollView>

      <View style={styles.buttonWidth}>
        <TouchableOpacity
          style={styles.buttons}
          onPress={() => {
            navigation.navigate("Home");
          }}
        >
          <Text style={styles.buttonText}>Return</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default addNewTeamMember;
