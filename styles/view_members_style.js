import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({ 
    buttons: {
        backgroundColor: "#8AAA79",
        alignItems: "center",
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 5,
    },
    buttonWidth: {
        marginRight: 30,
        marginLeft: 30,
        marginBottom: 10,
    },
    buttonText: {
        color: "#D1D5DE",
    },
    background:
    {
        backgroundColor: "#E5E5E5",
        paddingBottom: 300
    },
    container:
    {
        backgroundColor: "#B7B6C2",
        borderRadius: 5,
        marginRight: 28,
        marginLeft: 28,
        marginBottom: 10
    },
    scrollView: {
        alignItems: "flex-start",
        marginBottom: 0,
    },
    headerText:
    {
        marginTop: 120,
        fontSize: 33,
        marginLeft: 80,
        color: "#657153"
    },
    textBoxStyle:
    {
        borderRadius: 5,
        backgroundColor: "#D1D5DE",
        paddingLeft: 110,
        paddingRight: 90,
        marginBottom: 4,
        marginTop: 4,
        marginLeft: 40
    },
    textStyle:
    {
        paddingTop: 12,
        marginTop: -1,
        fontSize: 16,
        color: "#657153"
    },
    styleDelete: {
        paddingLeft: 120,
        marginTop: -40,
        marginBottom: 10,
        marginRight: -75
    },
    content:
    {
        alignItems: "center",
    }
});