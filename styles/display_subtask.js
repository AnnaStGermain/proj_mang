import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({ 
    buttons: {
        backgroundColor: "#8AAA79",
        alignItems: "center",
        alignSelf: "center",
        justifyContent: "center",
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 5,
        marginBottom: 5,
        paddingRight: 10,
        paddingLeft: 10,
        flexDirection: "column",
        marginRight: 5
    },
    buttonWidth: {
        width: 150,
        position: "relative",
        left: 45,
        marginBottom: 10,
    },
    buttonText: {
        color: "#D1D5DE",
    },
    background:
    {
        backgroundColor: "#E5E5E5",
        paddingBottom: 60
    },
    container:
    {
        backgroundColor: "#B7B6C2",
        borderRadius: 5,
        marginRight: 80,
        marginLeft: 80,
        marginBottom: 10,
        height: 100,
        width: 200
    },
    scrollView: {
        alignItems: "flex-start",
        marginBottom: -0,
        height: 100,
        width: 1000
    },
    headerText:
    {
        marginTop: 10,
        fontSize: 33,
        marginLeft: 0,
        color: "#657153",
        alignItems: "center"
    },
    textBoxStyle:
    {
        borderRadius: 5,
        backgroundColor: "#D1D5DE",
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: 4,
        marginTop: 4
    },
    textStyle:
    {
        fontSize: 15,
        color: "#657153"
    },
    content:
    {
        alignItems: "center",
        marginLeft: 0,
        marginBottom: 100,
        paddingBottom: 10,
        marginTop: 5
    }
});