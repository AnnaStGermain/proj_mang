import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({ 
    buttons: {
        backgroundColor: "#8AAA79",
        alignItems: "center",
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 5,
        marginBottom: 5,
        margin: 5,
        paddingLeft: 5,
        paddingRight: 5
    },
    buttonWidth: {
        marginRight: 30,
        marginLeft: 30,
        marginBottom: 10,
    },
    buttonText: {
        color: "#D1D5DE",
    },
    background:
    {
        backgroundColor: "#E5E5E5",
        paddingBottom: 60
    },
    container:
    {
        backgroundColor: "#B7B6C2",
        borderRadius: 5,
        marginRight: 28,
        marginLeft: 28,
        marginBottom: 10,
        height: "81%"
    },
    scrollView: {
        alignItems: "flex-start",
        height:"81%",
        flexGrow: 1
    },
    headerText:
    {
        marginTop: 60,
        fontSize: 30,
        alignItems: "center",
        marginLeft: 43,
        color: "#657153"
    },
    textBoxStyle:
    {
        borderRadius: 5,
        backgroundColor: "#D1D5DE",
        paddingLeft: 50,
        paddingRight: 50,
        marginBottom: 4,
        marginTop: 4,
        alignContent: "center"
    },
    textStyle:
    {
        fontSize: 15,
        color: "#657153",
    },
    content:
    {
        alignItems: "center",
        marginLeft: 50,
        marginBottom: -60,
        paddingBottom: 10
    }
});