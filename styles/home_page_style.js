import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({ 
    buttons: {
        backgroundColor: "#8AAA79",
        alignItems: "center",
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 5,
    },
    buttonWidth: {
        marginRight: 30,
        marginLeft: 30,
        marginBottom: 10,
    },
    buttonText: {
        color: "#D1D5DE",
    },
    background:
    {
        backgroundColor: "#E5E5E5",
        paddingBottom: 60
    },
    container:
    {
        backgroundColor: "#B7B6C2",
        borderRadius: 5,
        marginRight: 28,
        marginLeft: 28,
        marginBottom: 20
    },
    scrollView: {
        alignItems: "center",
        marginBottom: 210,
    },
    headerText:
    {
        marginTop: 60,
        fontSize: 33,
        marginLeft: 135,
        color: "#657153"
    },
    textBoxStyle:
    {
        borderRadius: 5,
        backgroundColor: "#D1D5DE",
        paddingLeft: 50,
        paddingRight: 50,
        marginBottom: 4,
        marginTop: 4
    },
    textStyle:
    {
        fontSize: 20,
        paddingTop: 10,
        paddingRight: 50,
        paddingLeft: 50,
        paddingBottom: 10,
        color: "#657153"
    },
    content:
    {
        alignItems: "center",
    }
});