import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({ 
    buttons: {
        backgroundColor: "#8AAA79",
        alignItems: "center",
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 5,
        paddingHorizontal: 10
    },
    buttonWidth: {
        marginRight: 30,
        marginLeft: 30,
        marginBottom: 10,
    },
    buttonText: {
        color: "#D1D5DE",
    },
    background:
    {
        backgroundColor: "#E5E5E5",
        paddingBottom: 60
    },
    container:
    {
        backgroundColor: "#B7B6C2",
        borderRadius: 5,
        marginRight: 28,
        marginLeft: 28,
        marginBottom: 10,
        height: "85%"
    },
    scrollView: {
        alignItems: "flex-start",
        height: "85%",
        flexGrow: 1
    },
    headerText:
    {
        marginTop: 60,
        fontSize: 33,
        marginLeft: 110,
        color: "#657153"
    },
    textBoxStyle:
    {
        borderRadius: 5,
        backgroundColor: "#D1D5DE",
        paddingLeft: 50,
        paddingRight: 50,
        marginBottom: 4,
        marginTop: 4
    },
    textStyle:
    {
        fontSize: 15,
        color: "#657153"
    },
    content:
    {
        alignItems: "center",
        marginLeft: 50,
        marginBottom: -150,
        paddingBottom: 10,
        marginTop: 5,
        flex: 1
    }
});