import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({ 
    buttons: {
        backgroundColor: "#8AAA79",
        alignItems: "center",
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 5,
    },
    buttonWidth: {
        marginRight: 30,
        marginLeft: 30,
        marginBottom: 10,
    },
    buttonText: {
        color: "#D1D5DE",
    },
    background:
    {
        backgroundColor: "#E5E5E5",
        paddingTop: 280,
        paddingBottom: 250
    },
    container:
    {
        backgroundColor: "#B7B6C2",
        borderRadius: 5,
        marginRight: 28,
        marginLeft: 28,
        marginBottom: 10
    },
    headerText:
    {
        marginTop: -15,
        fontSize: 33,
        marginLeft: 55,
        color: "#657153"
    },
    textBoxStyle:
    {
        borderRadius: 5,
        backgroundColor: "#D1D5DE",
        paddingLeft: 50,
        paddingRight: 50,
        marginBottom: 7,
        marginTop: 2
    },
    textStyle:
    {
        fontSize: 15,
        color: "#657153"
    },
    content:
    {
        alignItems: "center",
        marginTop: 13,
        marginBottom: 13
    }
});