import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import { Text, View, TouchableOpacity, ScrollView } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import styles from "./styles/home_page_style.js";

import DatabaseCreation from "./components/DatabaseCreation";
import NewProject from "./components/AddNewProject";
import ProjectLoad from "./components/LoadProjectInformation";
import DisplayProjectInformation from "./components/DisplayProjectInformation";
import AddTeamMember from "./components/AddTeamMember";
import AddProductBacklog from "./components/AddProjectBacklog";
import DisplayProjectBacklog from "./components/DisplayProductBacklog";
import DisplayTeamMembers from "./components/DisplayTeamMembers";
import DisplayUserStory from "./components/DisplayUserStory";
import DisplaySubTask from "./components/DisplaySubtask";
import db from "./components/Database";
let dbindex = 1;
let entryindex = 1;

function HomeScreen(props) {
  const [listOfNames, setListOfNames] = useState([]);
  const [projectEntryIndex, setProjectEntryIndex] = useState([]);
  const [projectDBIndex, setProjectDBIndex] = useState(1);
  const [projectDBIndecies, setProjectDBIndecies] = useState([]);
  const [userStoryID, setUserStoryID] = useState(1);
  const { navigation } = props;
  useEffect(() => {
    getIndecies();
  }, [props]);
  const handleNames = (name) => {
    setListOfNames(name);
  };
  const getIndecies = async () => {
    db.transaction((tx) => {
      tx.executeSql(
        "SELECT * FROM Projects",
        [],
        (_, { rows }) => {
          console.log("ROWS RETRIEVED!");

          let entries = rows._array;
          let count = 0;
          let list = [];
          entries.forEach((entry) => {
            console.log(entry);
            list[count] = entry.projects_id;
            count++;
          });
          setProjectDBIndecies(list);
        },
        (_, result) => {
          console.log("SELECT failed!");
          console.log(result);
        }
      );
    });
  };

  const projectInfoTransition = async (index) => {
    navigation.navigate("DisplayProjectInformation");
    setProjectDBIndex(projectDBIndecies[index]);
    setProjectEntryIndex(index);
    dbindex = projectDBIndex;
    entryindex = index + 1;
  };

  const handleUserStoryID = async (id) => {
    setUserStoryID(id);
  };

  return (
    <View style={styles.background}>
      <ProjectLoad listOfProjectNames={handleNames}></ProjectLoad>
      <StatusBar style="auto" />
      <Text style={styles.headerText}>Projects</Text>
      <ScrollView contentContainerStyle={styles.scrollView}
        style={styles.container}>
        {listOfNames.map((item, index) => {
          return (
            <View key={index} style={styles.textBoxStyle}>
            <TouchableOpacity
              key={index}
              onPress={() => projectInfoTransition(index)}
            >
              <Text key={index} style={styles.textStyle}>{item}</Text>
            </TouchableOpacity>
            </View>
          );
        })}
      </ScrollView>

      <View style={styles.buttonWidth}>
        <TouchableOpacity
          style={styles.buttons}
          onPress={() => {
            navigation.navigate("AddProject");
          }}
        >
          <Text style={styles.buttonText}>Add Project</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.buttonWidth}>
        <TouchableOpacity
          style={styles.buttons}
          onPress={() => {
            navigation.navigate("AddTeamMember");
          }}
        >
          <Text style={styles.buttonText}>Add Team Member</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.buttonWidth}>
        <TouchableOpacity
          style={styles.buttons}
          onPress={() => {
            navigation.navigate("DisplayTeamMembers");
          }}
        >
          <Text style={styles.buttonText}>Team Members</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <DatabaseCreation></DatabaseCreation>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: "Home",
            headerTitleStyle: {
              fontSize: 22,
            },
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AddProject"
          children={(props) => <NewProject />}
          options={{
            title: "Add Project",
            headerTitleStyle: {
              fontSize: 22,
            },
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="DisplayTeamMembers"
          children={(props) => <DisplayTeamMembers />}
          options={{
            title: "Display Team Members",
            headerTitleStyle: {
              fontSize: 22,
            },
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AddTeamMember"
          children={(props) => <AddTeamMember />}
          options={{
            title: "Add Team Member",
            headerTitleStyle: {
              fontSize: 22,
            },
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="DisplayProjectInformation"
          options={{ headerShown: false }}
          children={(props) => (
            <DisplayProjectInformation
              projDBIndex={dbindex}
              projIndex={entryindex}
            />
          )}
        />
        <Stack.Screen
          name="AddProductBacklog"
          children={(props) => (
            <AddProductBacklog projDBIndex={dbindex} projIndex={entryindex} />
          )}
          options={{
            title: "Add Product Backlog",
            headerTitleStyle: {
              fontSize: 22,
            },
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="DisplayProjectBacklog"
          options={{ headerShown: false }}
          children={(props) => (
            <DisplayProjectBacklog
              projDBIndex={dbindex}
              projIndex={entryindex}
            />
          )}
        />
        <Stack.Screen
          name="DisplayUserStory"
          options={{ headerShown: false }}
          children={(props) => (
            <DisplayUserStory projDBIndex={dbindex} projIndex={entryindex} />
          )}
        />
        <Stack.Screen
          name="DisplaySubtask"
          children={(props) => (
            <DisplaySubTask projDBIndex={dbindex} projIndex={entryindex} />
          )}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
